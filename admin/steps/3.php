<?php

use Bitrix\Main\Localization\Loc;

$iblock = new CIBlockElement;

$options_list = $options->get();

$table_id = "vkElementsList";

$sort = $request->get("sort") ?: "ID";
$order = $request->get("order") ?: "DESC";

$oSort = new CAdminSorting($table_id, $sort, $order);

$list = new CAdminList($table_id, $oSort);
$list->bMultipart = true;

$filter = ["IBLOCK_ID" => $options_list->iblock_id];
$request_filter = $request->get("filter");
if (!empty($request_filter)) {
    foreach ($request_filter as $key => $val) {
        $filter[$key] = $val;
    }
}

$navResult = new CAdminResult(null, '');
$arNavParams = array("nPageSize" => $navResult->GetNavSize(
            $table_id, array('nPageSize' => 20, 'sNavID' => $APPLICATION->GetCurPage() . '?IBLOCK_ID=' . $options_list->iblock_id))
);
unset($navResult);

$dbElementsList = $iblock->GetList([$sort => $order], $filter, false, $arNavParams);

$dbElementsList->SetTableID($table_id);

$dbElementsList->NavStart();
$list->NavText($dbElementsList->GetNavPrint("pages"));

$list->AddHeaders([
    [
        "id" => "ID",
        "content" => "ID",
        "title" => "",
        "sort" => "NAME",
        "default" => true
    ],
    [
        "id" => "NAME",
        "content" => Loc::getMessage("dbresky_VKEXPORTER_NAME_FIELD_TITLE"),
        "title" => "",
        "sort" => "NAME",
        "default" => true
    ],
    [
        "id" => "vk_active",
        "content" => Loc::getMessage("dbresky_VKEXPORTER_VK_ACTIVE_FIELD_TITLE"),
        "title" => "",
        "sort" => "",
        "default" => true
    ],
    [
        "id" => "vk_exported",
        "content" => Loc::getMessage("dbresky_VKEXPORTER_VK_EXPORTED_FIELD_TITLE"),
        "title" => "",
        "sort" => "",
        "default" => true
    ]
]);

while ($element = $dbElementsList->GetNextElement()) {

    $arr_fields = $element->GetFields();
    $arr_properties = $element->GetProperties();
    $row = $list->AddRow($arr_fields["ID"], $arr_fields);
}

$list->AddFooter(array(
    array("title" => Loc::getMessage("dbresky_VKEXPORTER_LIST_COUNT"), "value" => $dbElementsList->SelectedRowsCount()),
    array("counter" => true, "title" => Loc::getMessage("dbresky_VKEXPORTER_LIST_SELECTED_COUNT"), "value" => 0)
));

$list->CheckListMode();


$APPLICATION->SetTitle(Loc::getMessage("dbresky_VKEXPORTER_STEP3_TITLE"));

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

$list->DisplayList();

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");

