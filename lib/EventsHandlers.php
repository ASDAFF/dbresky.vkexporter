<?php

namespace dbresky\vkexporter;

/**
 * Обработчики событий
 *
 * @author ИП Бреский Дмитрий Игоревич <dimabresky@gmail.com>
 */
class EventsHandlers {

    public static function onAfterAddAlbum($id) {

        try {
            $album = tables\Albums::getTable()->getList(["filter" => ["ID" => $id]])->fetch();
            if (isset($album["ID"])) {
                $options = new Options;
                $gateway = new Gateway($options);
                $photo_id = null;
                if ($album["UF_PICTURE"] > 0) {
                    
                    $photo_id = $gateway->uploadAlbumImage($album["UF_PICTURE"]);
                }
                
                $gateway->addAlbum($album["UF_NAME"], $photo_id);
            }
        } catch (\Exception $ex){
            
            tables\Albums::getTable()->delete($id);
            throw new \Exception($ex->getMessage());
        }
    }

    public static function onAfterUpdateAlbum($parameters) {
        
    }

}
